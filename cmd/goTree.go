package main

import (
	"errors"
	"fmt"
	"go_tree/pkg/tree"
	"os"
)

func checkArgs() error {
	if !(len(os.Args) == 2 || len(os.Args) == 3) {
		fmt.Println("usage goTree . [-f]")
		return errors.New("no arguments provided")
	}
	return nil
}

func main() {
	out := os.Stdout

	err := checkArgs()
	if err != nil {
		return
	}

	path := os.Args[1]
	printFiles := len(os.Args) == 3 && os.Args[2] == "-f"
	tree.SkippedFolders = append(tree.SkippedFolders, ".idea")
	err = tree.DirTree(out, path, printFiles)
	if err != nil {

		panic(err.Error())
	}
}
