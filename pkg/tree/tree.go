package tree

import (
	"fmt"
	"io"
	"os"
)

// графические константы для отображения структуры дерева
const (
	innerMarker  = "├───"
	endingMarker = "└───"
	innerSpace   = "│   "
	endingSpace  = "    "
)

// SkippedFolders - срез папок, которые будут проигнорированы
var SkippedFolders = []string{".DS_Store"}

// DirTree строит дерево папок и файлов, результат выводит в out.
// path - путь с которого начинается построенае,
// printFiles - если false, то будут выведены только директории,
// если true, то вместе с директориями будут выведены файлы и их размеры.
func DirTree(out io.Writer, path string, printFiles bool) error {
	err := printFilesInDir(out, path, printFiles, "")
	if err != nil {
		return err
	}
	return nil
}

func printFilesInDir(out io.Writer, path string, printFiles bool, shift string) error {
	files, err := os.ReadDir(path)
	if err != nil {
		return err
	}

	fl, dir := sortFiles(files)

	if printFiles {
		for i := 0; i < len(fl); i++ {
			if isSkippedFolder(fl[i].Name()) {
				continue
			}
			fileSize := getFileSize(fl[i])

			if i == len(fl)-1 && len(dir) == 0 {
				outStr := fmt.Sprintf("%s%s%s (%s)\n", shift, endingMarker, fl[i].Name(), fileSize)
				_, err := out.Write([]byte(outStr))
				if err != nil {
					return err
				}
				continue
			}
			outStr := fmt.Sprintf("%s%s%s (%s)\n", shift, innerMarker, fl[i].Name(), fileSize)
			_, err := out.Write([]byte(outStr))
			if err != nil {
				return err
			}
		}
	}

	for i := 0; i < len(dir); i++ {
		if isSkippedFolder(dir[i].Name()) {
			continue
		}
		if i == len(dir)-1 {
			outStr := fmt.Sprintf("%s%s%s\n", shift, endingMarker, dir[i].Name())
			_, err := out.Write([]byte(outStr))
			if err != nil {
				return err
			}
			err = printFilesInDir(out, path+"/"+dir[i].Name(), printFiles, shift+endingSpace)
			if err != nil {
				return err
			}
			continue
		}

		outStr := fmt.Sprintf("%s%s%s\n", shift, innerMarker, dir[i].Name())
		_, err := out.Write([]byte(outStr))
		if err != nil {
			return err
		}
		err = printFilesInDir(out, path+"/"+dir[i].Name(), printFiles, shift+innerSpace)
		if err != nil {
			return err
		}
	}
	return nil
}

func sortFiles(files []os.DirEntry) (fl []os.DirEntry, dir []os.DirEntry) {
	for _, file := range files {
		if file.IsDir() {
			dir = append(dir, file)
			continue
		}
		fl = append(fl, file)
	}
	return
}

func isSkippedFolder(path string) bool {
	for _, passed := range SkippedFolders {
		if passed == path {
			return true
		}
	}
	return false
}

// getFileSize возвращает размер файлы в виде строки
func getFileSize(file os.DirEntry) string {
	fInfo, err := file.Info()
	if err != nil {
		return "unknown"
	}
	size := fInfo.Size()
	if size > 1024*10 {
		return fmt.Sprintf("%dkb", size/1024)
	}
	return fmt.Sprintf("%db", size)
}
